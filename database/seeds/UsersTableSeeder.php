<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'User One',
            'email' => 'user.one@tmail.test',
            'password' => bcrypt('password'),
        ]);
    }
}
